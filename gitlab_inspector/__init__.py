import os
import sys
import re
import datetime
import hashlib
import json
from types import SimpleNamespace

import click
import appdirs
from gitlab import Gitlab


class GitlabInspector:
    def __init__(self, gitlab_url, gitlab_token, project_path):
        gl = Gitlab(gitlab_url, private_token=gitlab_token)
        gl.auth()
        self.project = gl.projects.get(project_path)
        project_hash = hashlib.sha256()
        project_hash.update(f"{gitlab_url}\n{project_path}".encode())
        self.project_dir = os.path.join(
            appdirs.user_data_dir("gitlab-inspector"), project_hash.hexdigest()
        )
        os.makedirs(self.project_dir, mode=0o750, exist_ok=True)

    def list_jobs(self, batch, after, before, expr, regex, format):
        if regex is not None:
            regex = re.compile(regex, re.MULTILINE)
        pipelines_list_opts = {}
        if after is not None:
            pipelines_list_opts["updated_after"] = after
        if before is not None:
            pipelines_list_opts["updated_before"] = before
        for pipeline in self.project.pipelines.list(
            **pipelines_list_opts, iterator=True, per_page=batch
        ):
            for job in pipeline.jobs.list(
                include_retried=True,
                get_all=True,
            ):
                if job.finished_at is None:
                    continue
                job = job.attributes
                if expr is not None:
                    original_job = job
                    job = SimpleNamespace(**job)
                    if not eval(expr):
                        continue
                    job = original_job
                if regex is not None:
                    log = self.get_job_log(job["id"])
                    if regex.search(log) is None:
                        continue
                if format is None:
                    print(job["web_url"])
                else:
                    job = SimpleNamespace(**job)
                    print(eval(format))

    def show_job(self, job_id, format):
        job = self.project.jobs.get(job_id).attributes
        if format is None:
            print(json.dumps(job, indent=4, sort_keys=True))
        else:
            job = SimpleNamespace(**job)
            print(eval(format))

    def show_job_log(self, job_id):
        print(self.get_job_log(job_id))

    def get_job_log(self, job):
        if isinstance(job, (int, str)):
            job = self.project.jobs.get(job)
        job_log_path = os.path.join(self.project_dir, f"{job.id}.log")
        try:
            os.stat(job_log_path)
        except FileNotFoundError:
            with open(job_log_path, "wb") as job_log_file:
                job_log = job.trace()
                job_log_file.write(job_log)
        else:
            with open(job_log_path, "rb") as job_log_file:
                job_log = job_log_file.read()
        return job_log.decode("utf-8")


@click.group()
@click.option(
    "-u", "--url", envvar="GITLAB_URL", required=True, help="URL of GitLab instance"
)
@click.option(
    "-t", "--token", envvar="GITLAB_TOKEN", required=True, help="Personal access token"
)
@click.option("-p", "--project", required=True, help="Path to project")
@click.pass_context
def main(ctx, url, token, project):
    ctx.ensure_object(dict)
    ctx.obj["inspector"] = GitlabInspector(url, token, project)


@main.command("list")
@click.option("--batch", default=20, help="Pipelines batch size")
@click.option(
    "-a",
    "--after",
    default=None,
    type=click.DateTime(),
    help="Limit to pipelines updated after date",
)
@click.option(
    "-b",
    "--before",
    default=None,
    type=click.DateTime(),
    help="Limit to pipelines updated before date",
)
@click.option(
    "-e", "--expr", default=None, help="Filter jobs based on Python expression"
)
@click.option("-r", "--regex", default=None, help="Look for pattern in job logs")
@click.option(
    "-f", "--format", default=None, help="Format output with Python expression"
)
@click.pass_context
def list_jobs(ctx, batch, after, before, expr, regex, format):
    ctx.obj["inspector"].list_jobs(batch, after, before, expr, regex, format)


@main.command("show")
@click.argument("job_id", required=True)
@click.option(
    "-f", "--format", default=None, help="Format output with Python expression"
)
@click.pass_context
def show_job(ctx, job_id, format):
    ctx.obj["inspector"].show_job(job_id, format)


@main.command("show-log")
@click.argument("job_id", required=True)
@click.pass_context
def show_job_log(ctx, job_id):
    ctx.obj["inspector"].show_job_log(job_id)
