# gitlab-inspector

Inspect GitLab CI job results via API. It's like *grep*, but for GitLab pipelines.

> **CAUTION**: Job logs are fetched and cached locally. This may cause excessive stress on your network and GitLab instance.

# Features

- Search for phrases in job logs
- Filter jobs based on attributes
- Cache logs locally

# Usage

`GITLAB_TOKEN` environment variable must be set to your personal GitLab token with `read_api` scope.

List all jobs where log matches regular expression:

```
$ gitlab-inspector -p gitlab-org/gitlab-runner list --after $(date -I) --regex "^Error (occurred|happened)!"
```

Python's `re` module is used for compiling expressions.

List all failed jobs from time range:

```
$ gitlab-inspector -p gitlab-org/gitlab-runner list \
  --after '2022-01-01' \
  --before '2022-02-01' \
  --expr 'job.status == "failed"'
```

Show job's details:

```
# Only metadata without log
$ gitlab-inspector -p gitlab-org/gitlab-runner show 12043
$ gitlab-inspector -p gitlab-org/gitlab-runner show-log 12043
```
